package com.testtestonly.mvpsassignment.Views

import com.testtestonly.mvpsassignment.Models.UserModel

interface UserView:BaseView {
    fun onGetData(data:UserModel)
    fun onFailData(error:String)}