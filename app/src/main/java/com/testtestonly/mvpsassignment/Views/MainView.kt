package com.testtestonly.mvpsassignment.Views

import com.testtestonly.mvpsassignment.Models.UserBaseModel

interface MainView :BaseView{
    fun onSuceessData(data:ArrayList<UserBaseModel>)
    fun onFailData(error:String)
}