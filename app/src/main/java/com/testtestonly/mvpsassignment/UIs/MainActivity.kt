package com.testtestonly.mvpsassignment.UIs

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.testtestonly.mvpsassignment.Models.UserBaseModel
import com.testtestonly.mvpsassignment.Presenter.MainPresentIm
import kotlinx.android.synthetic.main.activity_main.*
import com.testtestonly.mvpsassignment.R
import com.testtestonly.mvpsassignment.Views.MainView
import kotlinx.android.synthetic.main.include_header_center_title.*

/**
 * Created Sep 2021
 */

class MainActivity : MainView, BaseActivity() {
    var listItems = ArrayList<UserBaseModel>()
    lateinit var mAdapter: MainAdapter
    var mPresenter = MainPresentIm<MainView>()
    override var isDestroy=false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mPresenter.onAttach(this)
        mPresenter.loadUsersAPI(false)
    }

    override fun initViews() {
        super.initViews()
        toolbar.title = getString(R.string.home_title)
        recycler_home.layoutManager = LinearLayoutManager(this)
        mAdapter = MainAdapter(listItems)
        recycler_home.adapter = mAdapter
        srl_home.setOnRefreshListener {
            mPresenter?.loadUsersAPI(true)
        }
    }

    override fun onSuceessData(data: ArrayList<UserBaseModel>) {
        data?.let {
            listItems.clear()
            listItems.addAll(it)
        }
        mAdapter.setData(listItems, true)
        srl_home?.isRefreshing = false

    }

    override fun onFailData(error: String) {
        srl_home?.isRefreshing = false
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.onDestroy()
    }
}