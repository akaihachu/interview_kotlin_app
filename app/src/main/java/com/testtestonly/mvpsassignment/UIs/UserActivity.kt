package com.testtestonly.mvpsassignment.UIs

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import com.testtestonly.mvpsassignment.Models.UserModel
import com.testtestonly.mvpsassignment.Presenter.UserPresentIm
import com.testtestonly.mvpsassignment.R
import com.testtestonly.mvpsassignment.Utils.Constants
import com.testtestonly.mvpsassignment.Utils.ImageGlider
import com.testtestonly.mvpsassignment.Views.UserView
import kotlinx.android.synthetic.main.activity_user.*
import kotlinx.android.synthetic.main.include_header_center_title.*

/**
 * Created Sep 2021.
 */

class UserActivity : UserView, BaseActivity() {
    var mPresenter = UserPresentIm<UserView>()
    var id: String? = ""
    override var isDestroy=false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)
        mPresenter.onAttach(this)
        id = intent?.getStringExtra(Constants.INTENT_KEY_STRING)
        id?.let { mPresenter.loadData(it, false) }
    }


    override fun initViews() {
        super.initViews()
        toolbar.title = getString(R.string.user_title)
        srl_user.setOnRefreshListener {
            id?.let {
                mPresenter.loadData(it, true)
            }
        }
    }

    override fun onGetData(data: UserModel) {
        data?.let {
            username_txt.text = it.name
            if (TextUtils.isEmpty(it.bio)){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    bio_txt.setTextColor(getColor(R.color.gray))
                    bio_txt.text= getString(R.string.bio_default_content)
                }
            } else {
                bio_txt.text=it.bio
                bio_txt.setTextColor(Color.BLACK)
            }
            location_txt.text = it.location
            followers_txt.text = it.followers.toString()
            following_txt.text = it.following.toString()
            repos_txt.text = it.public_repos.toString()
            it.avatar_url?.let {
                ImageGlider.loadBigImage(this, avatar_cimg, it)
            }
        }
        srl_user?.isRefreshing = false
    }

    override fun onFailData(error: String) {
        srl_user?.isRefreshing = false
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.onDestroy()
    }
}