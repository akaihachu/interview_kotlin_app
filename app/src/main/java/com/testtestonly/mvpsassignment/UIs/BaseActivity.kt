package com.testtestonly.mvpsassignment.UIs

import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import com.testtestonly.mvpsassignment.Views.BaseView
import kotlinx.android.synthetic.main.include_header_center_title.*

/**
 * Created Sep 2021
 */

abstract class BaseActivity : BaseView, AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
    }

    override fun setContentView(layoutResID: Int) {
        super.setContentView(layoutResID)
        initViews()
    }

    open fun initViews() {
        setSupportActionBar(toolbar)
        if (supportActionBar != null && !(this is MainActivity)) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }
        toolbar?.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}