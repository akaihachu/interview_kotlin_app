package com.testtestonly.mvpsassignment.Utils

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.request.RequestOptions
import com.testtestonly.mvpsassignment.GlideApp
import com.testtestonly.mvpsassignment.R

object ImageGlider {
    fun loadImage(context: Context, imageView: ImageView, url: String?) {
        val padding = imageView.context.resources.getDimension(R.dimen.padding_2).toInt()
        imageView.setPadding(padding, padding, padding, padding)
        GlideApp.with(context)
            .load(url)
            .apply(RequestOptions.circleCropTransform())
            .into(imageView)
    }

    fun loadBigImage(context: Context, imageView: ImageView, url: String?) {
        imageView.setBackgroundResource(R.drawable.circle_gray)
        val padding = imageView.context.resources.getDimension(R.dimen.padding_circle_user).toInt()
        imageView.setPadding(padding, padding, padding, padding)
        GlideApp.with(context)
            .load(url)
            .apply(RequestOptions.circleCropTransform())
            .into(imageView)
    }

}