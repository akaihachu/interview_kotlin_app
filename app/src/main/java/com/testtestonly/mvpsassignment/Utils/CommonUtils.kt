package com.testtestonly.mvpsassignment.Utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.widget.Toast
import com.testtestonly.mvpsassignment.R

class CommonUtils {
    companion object {
        fun test(context: Context) {
            context.getString(R.string.warning_offline)
        }

        protected const val CLICK_TIME_INTERVAL: Long = 800
        protected var mLastClickTime: Long = 0

        @JvmStatic
        fun checkClickTimeValidation(): Boolean {
            val now = System.currentTimeMillis()
            if (now - mLastClickTime < CLICK_TIME_INTERVAL) {
                return false
            }
            mLastClickTime = now
            return true
        }

        @JvmStatic
        fun isNetworkAvailable(context: Context?, withToast: Boolean = true): Boolean {
            if (context == null) {
                Toast.makeText(
                    context,
                    context?.getString(R.string.warning_offline),
                    Toast.LENGTH_LONG
                ).show()
                return false
            }
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                val capabilities =
                    connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
                if (capabilities != null) {
                    when {
                        capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                            return true
                        }
                        capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                            return true
                        }
                        capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                            return true
                        }
                    }
                }
            } else {
                var haveConnectedWifi = false
                var haveConnectedMobile = false
                val cm =
                    context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val netInfo = cm.allNetworkInfo
                for (ni in netInfo) {
                    if (ni.typeName.equals(
                            "WIFI",
                            ignoreCase = true
                        )
                    ) if (ni.isConnected) haveConnectedWifi = true
                    if (ni.typeName.equals(
                            "MOBILE",
                            ignoreCase = true
                        )
                    ) if (ni.isConnected) haveConnectedMobile = true
                }
                if (!(haveConnectedWifi || haveConnectedMobile)) {
                    android.widget.Toast.makeText(
                        context,
                        context?.getString(com.testtestonly.mvpsassignment.R.string.warning_offline),
                        android.widget.Toast.LENGTH_LONG
                    ).show()
                }
                return haveConnectedWifi || haveConnectedMobile
            }
            return false
        }
    }
}