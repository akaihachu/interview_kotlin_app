package com.testtestonly.mvpsassignment.APIs


import com.testtestonly.mvpsassignment.Models.UserBaseModel
import com.testtestonly.mvpsassignment.Models.UserModel
import retrofit2.http.*
import java.util.ArrayList
import retrofit2.http.GET
import io.reactivex.Observable


interface ApiInterface {

    @GET("users")
    fun users(): Observable<ArrayList<UserBaseModel>>

    @GET("users/{userId}")
    fun getUserDetail(
        @Path("userId") username: String
    ): Observable<UserModel>
}