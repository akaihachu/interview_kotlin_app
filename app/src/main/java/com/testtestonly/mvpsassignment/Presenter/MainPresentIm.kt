package com.testtestonly.mvpsassignment.Presenter

import androidx.appcompat.app.AppCompatActivity
import com.testtestonly.mvpsassignment.R
import com.testtestonly.mvpsassignment.Repos.DataRepository
import com.testtestonly.mvpsassignment.Repos.DiskDataSource
import com.testtestonly.mvpsassignment.Repos.MemoryDataSource
import com.testtestonly.mvpsassignment.Repos.NetworkDataSource
import com.testtestonly.mvpsassignment.Utils.Constants
import com.testtestonly.mvpsassignment.Views.MainView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MainPresentIm<V : MainView>() : MainPresenter<V>, BasePresentIm<V>() {

    override fun loadUsersAPI(isRefreshing: Boolean) {
        val memoryDataSource = MemoryDataSource()
        val diskDataSource = DiskDataSource(
            mContext.getSharedPreferences(
                Constants.SHARED_PREFERENCE_KEY,
                AppCompatActivity.MODE_PRIVATE
            )
        );
        val networkDataSource = NetworkDataSource()
        var repository = DataRepository(memoryDataSource, diskDataSource, networkDataSource)
        repository
            .getData(isRefreshing)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                mView?.onSuceessData(it)
            }, {
                mView?.onFailData(mContext.getString(R.string.warning_wrong))
            }
            )
    }

}