package com.testtestonly.mvpsassignment.Presenter

import androidx.appcompat.app.AppCompatActivity
import com.testtestonly.mvpsassignment.R
import com.testtestonly.mvpsassignment.Repos.DataRepository
import com.testtestonly.mvpsassignment.Repos.DiskDataSource
import com.testtestonly.mvpsassignment.Repos.MemoryDataSource
import com.testtestonly.mvpsassignment.Repos.NetworkDataSource
import com.testtestonly.mvpsassignment.Utils.Constants
import com.testtestonly.mvpsassignment.Views.UserView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class UserPresentIm<V : UserView> : UserPresenter<V>, BasePresentIm<V>() {

    override fun loadData(id: String, isRefreshing: Boolean) {
        val memoryDataSource = MemoryDataSource()
        val diskDataSource = DiskDataSource(
            mContext.getSharedPreferences(
                Constants.SHARED_PREFERENCE_KEY,
                AppCompatActivity.MODE_PRIVATE
            )
        );
        val networkDataSource = NetworkDataSource()
        var repository = DataRepository(memoryDataSource, diskDataSource, networkDataSource)
        repository
            .getDataUser(id, isRefreshing)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                mView?.onGetData(it)
            }, {
                mView?.onFailData(mContext.getString(R.string.warning_wrong))
            }
            )
    }
}