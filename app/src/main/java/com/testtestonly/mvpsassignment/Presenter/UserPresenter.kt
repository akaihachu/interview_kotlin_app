package com.testtestonly.mvpsassignment.Presenter

import com.testtestonly.mvpsassignment.Views.UserView

interface UserPresenter<V:UserView> : BasePresenter<V> {
    fun loadData(id: String, isRefresh: Boolean)
}