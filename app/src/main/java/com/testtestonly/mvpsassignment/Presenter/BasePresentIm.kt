package com.testtestonly.mvpsassignment.Presenter

import android.content.Context
import com.testtestonly.mvpsassignment.Views.BaseView

open class BasePresentIm<V:BaseView> : BasePresenter<V> {
    private var view: V? = null
    lateinit private var context: Context
    override fun onAttach(view: V?) {
        this.view = view
        context = view as Context
    }

    override fun onDestroy() {
        mView = null
        mView?.isDestroy=true
    }

    override var mView: V?
        get() = view
        set(value) {}
    override var mContext: Context
        get() = context
        set(value) {}

}