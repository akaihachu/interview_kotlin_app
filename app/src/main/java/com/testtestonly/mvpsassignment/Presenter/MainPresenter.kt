package com.testtestonly.mvpsassignment.Presenter

import com.testtestonly.mvpsassignment.Views.MainView

interface MainPresenter<V:MainView> : BasePresenter<V> {
    fun loadUsersAPI(isRefreshing: Boolean)
}