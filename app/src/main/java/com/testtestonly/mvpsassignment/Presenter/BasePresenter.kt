package com.testtestonly.mvpsassignment.Presenter

import android.content.Context
import com.testtestonly.mvpsassignment.Views.BaseView

interface BasePresenter<V:BaseView> {
    var mContext: Context
    var mView: V?
    fun onAttach(view: V?)
    fun onDestroy()
}