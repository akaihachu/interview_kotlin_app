package com.testtestonly.mvpsassignment.Repos

import com.testtestonly.mvpsassignment.Models.UserBaseModel
import com.testtestonly.mvpsassignment.Models.UserModel
import io.reactivex.Observable

interface DataSource {
    fun getData(isRefresh:Boolean=false): Observable<ArrayList<UserBaseModel>>
    fun getDataUser(id:String,isRefreshing:Boolean=false): Observable<UserModel>
}
