package com.testtestonly.mvpsassignment.Repos

import android.util.Log
import com.testtestonly.mvpsassignment.Models.UserBaseModel
import com.testtestonly.mvpsassignment.Models.UserModel
import io.reactivex.Observable

class MemoryDataSource : DataSource {

    override fun getDataUser(id: String, isRefresh: Boolean): Observable<UserModel> =
        Observable.create<UserModel> { emitter ->
            if (memory.get(id) != null) {
                emitter.onNext(memory.get(id)!!)
                Log.d("DATA SOURCE", "Load from Memory")
            } else {
                Log.d("DATA SOURCE", "Load from memory null")
            }
            emitter.onComplete()
        }
            .doOnNext {
                Log.d("DATA SOURCE", "Next Load from Memory" + it)
            }


    override fun getData(isRefresh: Boolean): Observable<ArrayList<UserBaseModel>> =
        Observable.create<ArrayList<UserBaseModel>> { emitter ->
            if (listMemory != null) {
                emitter.onNext(listMemory!!)
                Log.d("DATA SOURCE", "Load from Memory")
            } else {
                Log.d("DATA SOURCE", "Load from memory null")
            }
            emitter.onComplete()
        }
            .doOnNext {
                Log.d("DATA SOURCE", "Next Load from Memory" + it)
            }

    fun saveData(list: ArrayList<UserBaseModel>) {
        listMemory = list
    }

    fun saveDataUser(data: UserModel?) {
        data?.let {
            memory.put(it.login.toString(), it)
        }
    }

    companion object {
        private var memory = HashMap<String, UserModel>()
        var listMemory: ArrayList<UserBaseModel>? = null
    }
}
