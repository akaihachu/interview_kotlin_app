package com.testtestonly.mvpsassignment.Repos

import android.util.Log
import com.testtestonly.mvpsassignment.APIs.ApiClient
import com.testtestonly.mvpsassignment.Models.UserBaseModel
import io.reactivex.Observable

import com.testtestonly.mvpsassignment.Models.UserModel

class NetworkDataSource : DataSource {

    override fun getData(isRefresh: Boolean): Observable<ArrayList<UserBaseModel>> {
        var apiService = ApiClient.`interface`.users()
            .doOnNext {
                Log.d("DATA SOURCE", "Next Load from network")
            }
        return apiService
    }

    override fun getDataUser(id: String, isRefresh: Boolean): Observable<UserModel> {
        var apiService = ApiClient.`interface`.getUserDetail(id)
            .doOnNext {
                Log.d("DATA SOURCE", "Next Load from network")
            }
        return apiService
    }
}
