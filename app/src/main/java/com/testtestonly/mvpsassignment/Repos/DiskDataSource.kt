package com.testtestonly.mvpsassignment.Repos

import android.content.SharedPreferences
import android.util.Log
import com.google.gson.Gson
import com.testtestonly.mvpsassignment.Models.UserBaseModel
import com.testtestonly.mvpsassignment.Models.UserModel
import com.testtestonly.mvpsassignment.Utils.Constants
import io.reactivex.Observable
import com.google.gson.reflect.TypeToken


class DiskDataSource(private val preferences: SharedPreferences) : DataSource {

    override fun getData(isRefresh: Boolean): Observable<ArrayList<UserBaseModel>> =
        Observable.create<ArrayList<UserBaseModel>> {
            val dataUserJson = preferences.getString(Constants.SHARED_PREFERENCE_LIST_USER, null)
            if (dataUserJson != null) {
                Log.d("DATA SOURCE", "Load from disk")

                var gson = Gson()
                var dataUser = gson.fromJson(
                    dataUserJson,
                    object : TypeToken<ArrayList<UserBaseModel>>() {}.type
                ) as ArrayList<UserBaseModel>
                it.onNext(dataUser)
            } else {
                Log.d("DATA SOURCE", "Load from disk null")
            }
            it.onComplete()

        }

    override fun getDataUser(id: String, isRefresh: Boolean): Observable<UserModel> =
        Observable.create<UserModel> {
            val dataUserJson =
                preferences.getString(Constants.SHARED_PREFERENCE_USERBASEMODEL + id, null)
            if (dataUserJson != null) {
                Log.d("DATA SOURCE", "Load from disk")

                var gson = Gson()
                var dataUser: UserModel = gson.fromJson(dataUserJson, UserModel::class.java)
                it.onNext(dataUser)
            } else {
                Log.d("DATA SOURCE", "Load from disk null")
            }
            it.onComplete()

        }

    fun saveData(data: ArrayList<UserBaseModel>?) {
        val json = Gson().toJson(data)
        data?.let {
            preferences.edit().putString(Constants.SHARED_PREFERENCE_LIST_USER, json).apply()
        }
    }

    fun saveDataUser(dataUser: UserModel?) {
        val json = Gson().toJson(dataUser)
        if (dataUser != null) {
            preferences.edit().putString(
                Constants.SHARED_PREFERENCE_USERBASEMODEL + dataUser.login.toString(),
                json
            ).apply()
        }
    }
}
